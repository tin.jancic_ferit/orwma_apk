 package com.example.outpost;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

 public class SecondActivity extends AppCompatActivity {

    ImageView mainImageView;
    TextView description, title;

    String data4, data3;
    int boardImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        mainImageView = findViewById(R.id.mainImageView);
        description = findViewById(R.id.description);
        title = findViewById(R.id.title);

        getData();
        setData();
    }

    private void getData(){
        if(getIntent().hasExtra("images") && getIntent().hasExtra("data3")
        && getIntent().hasExtra("data4")){

            data4 = getIntent().getStringExtra("data4");
            data3 = getIntent().getStringExtra("data3");
            boardImage = getIntent().getIntExtra("images", 1);

        }else{
            Toast.makeText(this, "No data.", Toast.LENGTH_SHORT).show();
        }
    }

    private void setData(){
        description.setText(data4);
        title.setText(data3);
        mainImageView.setImageResource(boardImage);
    }
}