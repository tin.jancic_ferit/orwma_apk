package com.example.outpost;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    String sColor[], sFirst[], sName[], sLines[];
    int images[] = {R.drawable._ruy_lopez,
                    R.drawable._italian_game,
                    R.drawable._four_knights,
                    R.drawable._scotch_game,
                    R.drawable._queens_gambit_accepted,
                    R.drawable._queens_gambit_declined,
                    R.drawable._london_system,
                    R.drawable._kings_indian,
                    R.drawable._reti_opening,
                    R.drawable._english_opening,
                    R.drawable._birds_opening,
                    R.drawable._sicilian_defense,
                    R.drawable._dutch_defense,
                    R.drawable._stafford_gambit,
                    R.drawable._budapest_gambit,
                    R.drawable._pirc_defense,
                    R.drawable._alekhines_defense,
                    R.drawable._benoni_defense,
                    R.drawable._slav_defense,
                    R.drawable._benko_gambit};
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = findViewById(R.id.recyclerView);

        sColor = getResources().getStringArray(R.array.color);
        sFirst = getResources().getStringArray(R.array.first_move);
        sName = getResources().getStringArray(R.array.openings_name);
        sLines = getResources().getStringArray(R.array.lines);

        MyAdapter myAdapter = new MyAdapter(this, sColor, sFirst, sName, sLines, images);
        recyclerView.setAdapter(myAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
}
